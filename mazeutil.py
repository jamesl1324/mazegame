#!/usr/bin/python3
# -*- coding: utf-8 -*-


def getmaze(map='maps/maze.txt'):  # Default map is maze.txt
    mazefile = open(map, 'r')  # open the usr specified file
    maze = []  # creates the 2D array/list

    for line in mazefile:
        buffer = []
        for ltr in line:

            buffer.append(ltr)


        maze.append(buffer[:-1])

    return maze


def main():
    print(len(getmaze()), len(getmaze()[0]))
    print(getmaze())


if __name__ == '__main__':
    main()


