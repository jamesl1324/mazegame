README
======

### About ###

This is a text based maze game made using Python and the default graphics framework TKInter.

Current Version: 1.4.6

### Set Up ###

Dependencies so far:

- Python 3 and above
- TKInter for Python 3 and above

To run, simply execute the main.py python script.

To install TKInter for Python 3:

- Ubuntu & flavours: sudo apt-get install python3-tk
- Fedora: dnf install tkinter

to create your own maze, create a text file with the design of the maze beginning at the very start of the text document, with each row on a newline.

\# will be treated as a wall.

x will be the starting position of the player. It will also represent the character in game.

/ will be the exit of the maze.

### About the game ###

The chequered square is the finish. You are the blue dot. Reach the finish to win. 

#### Controls ####

- Up Arrow = move character up
- Down Arrow = move character down
- Left Arrow = move character left
- Right Arrow = move character right
- R = restart
- Escape = exit
- Enter = start game/restart after win
- Q = exit after winning



### Contact information for suggestions and bug reports ###

Email: jamesl1324@hotmail.com