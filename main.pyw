#!/usr/bin/python3
# -*- coding: utf-8 -*-

import mazeutil
import time
import os

try:  # In case tkinter not installed on Linux for example
    from tkinter import *
    from tkinter import filedialog
    from tkinter import ttk
except ImportError:
    print('Error 01: Please install TKInter for Python 3.')
    time.sleep(3)  # gives user time to see the error
    sys.exit()


def clear():  # Clears the console
    if os.name == 'posix':  # Checks type of os
        os.system('clear')  # Calls a clear command
    elif os.name == 'nt':
        os.system('cls')


class Maze(object):  # Class for mazes
    def __init__(self):
        self.maze = mazeutil.getmaze()  # gets default maze

    def getsize(self):
        return len(self.maze), len(self.maze[0])  # grabs the height, then width, and returns as a tuple

    def getcoords(self):  # Searches for the x, which is the character
        y = 0
        x = 0
        for xlist in self.maze:  # 'For each sublist'
            for value in xlist:
                if value == 'x':  # If it's the character, return it's co-ordinates
                    return x, y
                else:             # Otherwise, add 1 onto x since it's going through sublists
                    x += 1
            else:
                x = 0  # Resets the x when it reaches a new 'y' co-ordinate

            y += 1  # Increments the y co-ordinate
        else:
            raise IndexError

    #def update(self, new_coords):  # Simply changes the map
    #    old_coords = self.getcoords()  # finds where the player was before the change
    #
    #    if self.maze[new_coords[1]][new_coords[0]] == '.':  # if the next step is a path then move the player
    #        self.maze[old_coords[1]][old_coords[0]] = '.'
    #        self.maze[new_coords[1]][new_coords[0]] = 'x'
    #    elif self.maze[new_coords[1]][new_coords[0]] == '/':  # if the next step is the finish line, pass
    #        pass

    def move(self, destination):  # Checks validity of inputs, then calls the raw movement

        if self.maze[destination[1]][destination[0]] == '.':  # Valid path
            #self.update(destination)
            pass
        elif self.maze[destination[1]][destination[0]] == '/':  # Win condition
            return 'WIN'
        elif self.maze[destination[1]][destination[0]] == '#':  # Raises ValueError as new map values are invalid
            raise ValueError  # both are ignored, so essentially pass, but the controls needs a response.
        else:
            raise ValueError


class Gui(object):  # Everything to do with GUI
    def __init__(self, root):  # root is just a Tk() object, size is the size of the maze map
        self.root = root
        self.mazesize = (1, 1)  # mazesize is used for swapping character and path

        self.root.title('Maze')

        self.mazelocation = str()  # Used for maze selection
        self.custommaze = False  # Determines whether default map is used

        self.wall = PhotoImage(file="assets/wall.gif")  # 32x32 gifs used for assets
        self.path = PhotoImage(file="assets/path.gif")
        self.finish = PhotoImage(file='assets/finish.gif')
        self.character = PhotoImage(file='assets/character.gif')

        self.frame = ttk.Frame(self.root)                     #
        self.frame.grid(column=0, row=0, sticky=(N, W, E, S)) #
        self.frame.columnconfigure(0, weight=1)               # Frame initialisation
        self.frame.rowconfigure(0, weight=1)                  #
        self.frame.grid_configure(padx=5, pady=5)             #

        ttk.Label(self.frame, text='Up = move up').grid(column=1, row=1)  # Controls screen
        ttk.Label(self.frame, text='Down = move down').grid(column=1, row=2)
        ttk.Label(self.frame, text='Left = move left').grid(column=1, row=3)
        ttk.Label(self.frame, text='Right = move right').grid(column=1, row=4)
        ttk.Label(self.frame, text='R = restart').grid(column=1, row=5)
        ttk.Label(self.frame, text='').grid(column=1, row=6)
        ttk.Label(self.frame, text='Press Enter to start.').grid(column=1, row=7)
        ttk.Button(self.frame, text='Choose maze', command=self.findmaze).grid(column=1, row=8)  # Brings up filedialog

    def win(self, message, time, moves):  # Window which pops up when win condition is met
        window = Toplevel(self.frame)  # Creates a new window
        window.title('Alert')

        time = 'Your time was: {} seconds.'.format(time)  # puts arguments in a readable format
        moves = 'You took {} moves to complete the maze.'.format(moves)

        ttk.Label(window, text=message).grid(column=1, row=1, columnspan=2)
        ttk.Label(window, text=time).grid(column=1, row=2, columnspan=2)
        ttk.Label(window, text=moves).grid(column=1, row=3, columnspan=2)
        ttk.Button(window, text='Yay! (q to quit)', command=self.destroy).grid(column=1, row=4, columnspan=1)  # Quit
        ttk.Label(window, text='Press R or Enter to restart.').grid(column=2, row=4, columnspan=1)

        for child in window.winfo_children():
            child.grid_configure(padx=30, pady=15)  # Adds some padding to each widget

        window.focus_force()  # Focus on the new window

        window.bind('<q>', self.destroy)  # key binding

    def rendermaze(self, maze):  # Maze is the raw maze list
        for y in range(self.mazesize[0]):  # Gets the y co-ordinate
            counter = 0  # used for index, so this should be x

            for x in maze[y]:  # for item in sublist at y co-ordinate
                # sets labels to assets with no spaces between.
                if x == '#':
                    # Adds label with wall texture
                    Label(self.frame, image=self.wall, borderwidth=0, highlightthickness=0).grid(column=counter, row=y)

                elif x == '.':
                    Label(self.frame, image=self.path, borderwidth=0, highlightthickness=0).grid(column=counter, row=y)

                elif x == '/':
                    Label(self.frame, image=self.finish, borderwidth=0, highlightthickness=0).grid(column=counter, row=y)

                elif x == 'x':
                    Label(self.frame, image=self.character, borderwidth=0, highlightthickness=0).grid(column=counter, row=y)

                else:
                    ttk.Label(self.frame, text=x).grid(column=counter, row=y)

                counter += 1

    def clearmaze(self):  # Doesn't really matter now, but still used to reset maze
        for child in self.frame.winfo_children():
            child.destroy()  # Destroy each widget

    def findmaze(self, *args):  # Used for loading custom mazes
        self.mazelocation = filedialog.askopenfilename()
        self.custommaze = True

    def move(self, x, y, nx, ny):  # original x & y, new x & y
        lx, ly = 0, 0  # local x & y, used as counters
        mx = self.mazesize[0]  # x limit

        for child in self.frame.winfo_children():
            if lx == x and ly == y:  # if counters match the co-ords of the character square
                child.config(image=self.path)  # changes to path
            elif lx == nx and ly == ny:  # if counters match the co-ords of the destination
                child.config(image=self.character)  # changes to character

            if lx + 1 != mx:  # if x counter can still increase
                lx += 1  # then increase the counter
            else:  # otherwise, move onto a new row
                lx = 0
                ly += 1

    def destroy(self, *args):
        self.root.destroy()


class Controls(object):
    def __init__(self, mazeclass, gui):  # The arguments are instances of Maze and Gui class. Should really be called
        self.m = mazeclass               # Interface instead of Controls.
        self.x = 0  # The position of the character.
        self.y = 0
        self.gui = gui
        self.gui.mazesize = self.m.getsize()  # sets the GUI class mazesize to the size of the maze

        self.timer = 0  # Game stats, needs to be here due to creating win window here with these stats.
        self.moves = 0
        self.totalmoves = 0

    def refresh(self, *args):  # Refreshing the maze
        if len(args) > 0:  # if co-ordinates are passed
            self.gui.move(args[0], args[1], args[2], args[3])
        else:  # usually only called if win condition reached
            self.gui.clearmaze()
            self.gui.rendermaze(self.m.maze)

        self.gui.root.bind_all('<Return>', lambda x: False)
        # Need to use this instead of unbind after binding for some reason?

    def up(self, *args):  # *args because it's a key binding function.
        if self.y - 1 < 0:  # Checks the character won't be out of the map bounds after move
            pass
        else:
            self.movement(0, 1)  # Pass the changes to x and y to movement function

    def down(self, *args):
        if self.y + 1 > self.m.getsize()[0]:
            pass
        else:
            self.movement(0, -1)

    def left(self, *args):
        if self.x - 1 < 0:
            pass
        else:
            self.movement(1, 0)

    def right(self, *args):
        if self.x + 1 > len(self.m.maze[self.x]):
            pass
        else:
            self.movement(-1, 0)

    def movement(self, x, y):  # x & y are offsets
        try:  # if something goes wrong...
            if self.m.move((self.x - x, self.y - y)) == 'WIN':  # if next square is the finish
                self.refresh()
                self.moves += 1
                self.totalmoves += 1
                self.gui.win('You Win!', str(round(time.time() - self.timer, 3)), self.moves)  # calls win screen with stats
                self.gui.root.bind_all('<Return>', self.restart)
            else:
                self.x, self.y = self.x - x, self.y - y  # applies offset first
                self.refresh(self.x + x, self.y + y, self.x, self.y)  # then pass the original and new co-ords
                self.moves += 1
                self.totalmoves += 1
        except ValueError:  # ...do nothing
            pass
        except IndexError:
            pass

    def restart(self, *args):

        if self.gui.custommaze:  # grabs maze again
            self.m.maze = mazeutil.getmaze(self.gui.mazelocation)  # If a custom map has been selected
        else:
            self.m.maze = mazeutil.getmaze()  # Otherwise use default value

        self.refresh()  # reloads map

        self.x, self.y = self.m.getcoords()  # reset character position

        self.moves = 0  # reset game stats
        self.timer = time.time()

    def start(self, *args):
        if self.gui.custommaze:  # Checks map condition
            self.m.maze = mazeutil.getmaze(self.gui.mazelocation)
        else:
            self.m.maze = mazeutil.getmaze()

        self.gui.mazesize = self.m.getsize()  # sets based on the map chosen
        self.x, self.y = self.m.getcoords()

        self.refresh()  # inits the display

        self.gui.root.unbind('<Return>')  # binding controls
        self.gui.root.bind('<Left>', self.left)
        self.gui.root.bind('<Right>', self.right)
        self.gui.root.bind('<Up>', self.up)
        self.gui.root.bind('<Down>', self.down)
        self.gui.root.bind_all('<r>', self.restart)

        self.timer = time.time()  # starts game time

    def getmoves(self):  # Just a wrapper
        return self.totalmoves


def main():
    if sys.version_info[0] == 2:  # Doesn't use Python 2
        print('Error 02: Incompatible Python version.')
        time.sleep(3)
        sys.exit()

    inittimer = time.time()  # Starts program timer

    clear()  # clears console if launched from terminal

    m = Maze()  # Creating instances

    app = Gui(Tk())

    ct = Controls(m, app)  # passing created instances

    for widget in app.frame.winfo_children():  # adds padding to controls screen widgets
        widget.grid_configure(padx=15, pady=5)

    app.root.bind_all('<Escape>', app.root.destroy)  # adds bindings for controls screen and whole app
    app.root.bind('<Return>', ct.start)

    app.root.mainloop()

    print('\n', 'Total runtime: {} seconds.\n'.format(round((time.time() - inittimer), 2), ))  # app duration stats
    print('\n', 'Total moves: {}.\n'.format(ct.getmoves()))


if __name__ == '__main__':
    main()
